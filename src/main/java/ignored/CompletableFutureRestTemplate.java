package ignored;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * Created by Mohamed on 25-Jan-19
 */
public class CompletableFutureRestTemplate {
    private RestTemplate restTemplate;

    public CompletableFutureRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CompletableFutureRestTemplate() {
        this.restTemplate = new RestTemplate();
    }

    public <T> CompletableFuture<ResponseEntity<T>> exchange(
        final String url,
        final HttpMethod method,
        final HttpEntity<?> body,
        final Class<T> returnType
    ) {
        final Supplier<ResponseEntity<T>> exchangeSupplier =
            () -> restTemplate.exchange(url, method,  body, returnType);

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    public <T> CompletableFuture<ResponseEntity<T>> exchange(
        final String url,
        final HttpMethod method,
        final HttpEntity<?> body,
        final ParameterizedTypeReference<T> typeReference
    ) {
        final Supplier<ResponseEntity<T>> exchangeSupplier =
            () -> restTemplate.exchange(url, method,  body, typeReference);

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    public <T> CompletableFuture<T> get(
        final String url,
        final Class<T> returnType
    ) {
        final Supplier<T> exchangeSupplier =
            () -> returnSafe(this.exchange(url, HttpMethod.GET,  null, returnType).join());

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    public <T> CompletableFuture<T> get(
        final String url,
        final ParameterizedTypeReference<T> typeReference
    ) {
        final Supplier<T> exchangeSupplier =
            () -> returnSafe(this.exchange(url, HttpMethod.GET,  null, typeReference).join());

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    public <T> CompletableFuture<T> post(
        final String url,
        final HttpEntity<?> body,
        final Class<T> returnType
    ) {
        final Supplier<T> exchangeSupplier =
            () -> returnSafe(this.exchange(url, HttpMethod.GET,  body, returnType).join());

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    public <T> CompletableFuture<T> post(
        final String url,
        final HttpEntity<?> body,
        final ParameterizedTypeReference<T> typeReference
    ) {
        final Supplier<T> exchangeSupplier =
            () -> returnSafe(this.exchange(url, HttpMethod.GET,  body, typeReference).join());

        return CompletableFuture.supplyAsync(exchangeSupplier);
    }

    private <T> T returnSafe(final ResponseEntity<T> requestResult) {
        if (Objects.isNull(requestResult) || Objects.isNull(requestResult.getBody())) {
            return null;
        }
        return requestResult.getBody();
    }
}

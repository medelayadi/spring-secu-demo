package ignored.stackoverflow;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * Created by Mohamed on 25-Jan-19
 */
public class BearRuleTest extends AnimalRuleTest<BearRule> {

    public static void main(String...strings) throws Exception {

        final ParameterizedType bearRuleTestGenericSuperclass =
            (ParameterizedType) BearRuleTest.class.getGenericSuperclass();
        final Type type = bearRuleTestGenericSuperclass.getActualTypeArguments()[0];
        final ParameterizedType animalRuleGenericSuperclass =
            (ParameterizedType) Class.forName(type.getTypeName()).getGenericSuperclass();

        System.out.println(
            animalRuleGenericSuperclass.getActualTypeArguments()[0]
        );
    }
}

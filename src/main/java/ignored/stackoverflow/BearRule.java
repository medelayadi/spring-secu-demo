package ignored.stackoverflow;

import org.springframework.core.ParameterizedTypeReference;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * Created by Mohamed on 25-Jan-19
 */
public class BearRule extends AnimalRule<Bear> {


    public static void main(String...strings) {

        final ParameterizedType bearRuleGenericSuperclass = (ParameterizedType) BearRule.class.getGenericSuperclass();


        Arrays.stream(bearRuleGenericSuperclass.getActualTypeArguments())
            .forEach(System.out::println);
        System.out.println();


//        System.out.println((Class)
//            ((ParameterizedType)BearRule.class.getGenericSuperclass())
//                .getActualTypeArguments()[0]);
    }
}

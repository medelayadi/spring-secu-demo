package ignored;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mohamed on 24-Jan-19
 */
public class Main2 {
    public static void main(String...strings) {

        final CompletableFuture[] completableFutures = IntStream.range(0, 10)
            .mapToObj(i -> _task(i))
            .toArray(CompletableFuture[]::new);
        Long now = System.currentTimeMillis();
        CompletableFuture.allOf(completableFutures)
            .thenRunAsync(() -> System.out.println("Ended doing things"))
            .join();


//        CompletableFuture.allOf(completableFutures)
//            .thenApplyAsync(aVoid -> {
//                Arrays.stream(completableFutures)
//                    .forEach(completableFuture -> {
//                        System.out.println(completableFuture.join());
//                    });
//                return null;
//            })
//            .join();
        System.out.println(System.currentTimeMillis()-now);
    }

    private static CompletableFuture<String> _task(final int index) {
        System.out.println("-- task " + index);
        return CompletableFuture.supplyAsync(() -> "-- task " + index);
    }

    private static void runAndLog(final int index) {
        System.out.println(_task(index).join());
    }
}

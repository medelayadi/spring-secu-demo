package ignored;

import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/**
 * Created by Mohamed on 25-Jan-19
 */
public class Main4 {
    private static RestTemplate restTemplate = new RestTemplate();
    public static void main(String...strings) {
        final CompletableFutureRestTemplate api = new CompletableFutureRestTemplate(restTemplate);

        api.get(dummyUrl(1), Main3.Dummy.class)
            .thenCombineAsync(
                api.get(dummyUrl(2), Main3.Dummy.class),
                (d1, d2) -> {
                    System.out.println("d1 = " + d1);
                    System.out.println("d2 = " + d2);
                    return null;
                }
            )
            .join()
        ;
    }

    static final String dummyUrl(final int id) {
        return "https://jsonplaceholder.typicode.com/todos/" + id;
    }
}

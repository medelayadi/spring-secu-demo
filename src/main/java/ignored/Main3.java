package ignored;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

/**
 * Created by Mohamed on 24-Jan-19
 */
public class Main3 {
    private static RestTemplate restTemplate = new RestTemplate();
    public static void main(String...strings) {
        final CompletableFuture<Dummy> dummy1 =  getForObject("https://jsonplaceholder.typicode.com/todos/1", Dummy.class, true);
        final CompletableFuture<Dummy> dummy2 =  getForObject("https://jsonplaceholder.typicode.com/todos/2", Dummy.class, false);
        CompletableFuture
            .allOf(dummy1,dummy2)
            .join();
        System.out.println(dummy1.join());
        System.out.println(dummy2.join());
    }

    private static <T> CompletableFuture<T> getForObject(final String url, final Class<T> returnType, final boolean delay) {
        final Supplier<T> getSupplier = () -> restTemplate.getForObject(url, returnType);
        return CompletableFuture
            .supplyAsync(getSupplier)
            .handle((t, throwable) -> {
                if (!Objects.isNull(throwable)) {
                    return null;
                }
                if (delay) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return t;
            })
            ;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Dummy {
        private int id;
        private int userId;
        private String title;
        private boolean completed;
    }
}

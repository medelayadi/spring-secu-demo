package ignored;

import org.mel.learn.secu.secu.model.User;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mohamed on 24-Jan-19
 */
public class Main1 {
    public static void main(String...strings) throws Exception {
        final Long now = System.currentTimeMillis();

        final Supplier<User> makeUserSupplier = () -> makeUser();
        final Supplier<String> uuidSupplier =  () -> makeUUID();

        final CompletableFuture<User> futureUser = CompletableFuture.supplyAsync(makeUserSupplier);
        final CompletableFuture<String> futureUUID = CompletableFuture.supplyAsync(uuidSupplier);

//        CompletableFuture.allOf(futureUser, futureUUID)
//            .thenApplyAsync(aVoid -> {
//                final User user = futureUser.join();
//                final String uuid = futureUUID.join();
//                return "received user + " + user + " and uuid is " + uuid ;
//            })
//            .handle((ok, e) -> {
//                System.out.println("ok----" + ok);
//                System.out.println("e----" + e);
//                return null;
//            }).join();

        futureUser.thenCombineAsync(futureUUID, (user, uuid) -> {
            return "received user + " + user + " and uuid is " + uuid;
        }).handle((ok, e) -> {
            System.out.println("ok----" + ok);
            System.out.println("e----" + e);
            return null;
        }).join();

        System.out.println(System.currentTimeMillis() - now);
    }
    private static User makeUser() throws RuntimeException {
//        throw new RuntimeException("lkj");
        return new User(1L, "mm", "ll", "kk");
    }
    private static String makeUUID() throws RuntimeException {
        return UUID.randomUUID().toString();
//        return "dummy";
    }
}

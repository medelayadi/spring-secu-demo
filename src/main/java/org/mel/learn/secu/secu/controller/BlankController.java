package org.mel.learn.secu.secu.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by Mohamed on 23-Jan-19
 */
@RestController
@RequestMapping("/")
public class BlankController {

    @GetMapping
    public ResponseEntity<String> getBlank(Principal principal) {
        return ResponseEntity.ok("Hello, world!!\n from "+principal.getName());
    }

    @GetMapping("admin")
    public ResponseEntity<String> getAdminBlank() {
        return ResponseEntity.ok("Hello, Admin!!");
    }
}

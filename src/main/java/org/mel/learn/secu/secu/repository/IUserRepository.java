package org.mel.learn.secu.secu.repository;

import org.mel.learn.secu.secu.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}

package org.mel.learn.secu.secu.service;

import org.mel.learn.secu.secu.core.CustomUserPrincipal;
import org.mel.learn.secu.secu.model.User;
import org.mel.learn.secu.secu.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by Mohamed on 23-Jan-19
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    IUserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User nestedUser = userRepository.findByUsername(username);
        if (Objects.isNull(nestedUser)) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserPrincipal(nestedUser);
    }
}
